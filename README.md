# Overview

This is the root repository to be used on the edge using the [Anthos Consumer Edge solution](https://gitlab.com/gcp-solutions/app-mod/sm002-consumer-edge/consumer-retail-edge/abm-consumer-edge-ansible)


## Installed by Default
- Longhorn
- CDI for KubeVirt
- KubeVirt VNC Lite (no-vnc)
- Default echo service (default namespace)
- External Secrets


### ACM Overview

See [our documentation](https://cloud.google.com/anthos-config-management/docs/repo) for how to use each subdirectory.
just read