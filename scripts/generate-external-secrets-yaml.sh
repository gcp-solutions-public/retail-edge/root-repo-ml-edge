#!/bin/bash

### dependencies
# * Helm 3.x
# * Kubernetes Split - https://github.com/mogensen/kubernetes-split-yaml/releases/download/v0.3.0/kubernetes-split-yaml_0.3.0_linux_amd64.tar.gz

ERROR=0
if [[ ! -x $(command -v kubernetes-split-yaml) ]]; then
    echo "ERROR: kubernetes-split-yaml is required. Please install: https://github.com/mogensen/kubernetes-split-yaml"
    ERROR=1
fi

if [[ ! -x $(command -v helm) ]]; then
    echo "Helm (helm) is required. Please install and retry"
    ERROR=1
fi

if [[ "${ERROR}" -eq 1 ]]; then
    echo "Required applications are not present on this host machine. Please install and re-try"
    exit 1
fi


helm repo add external-secrets https://charts.external-secrets.io

helm template external-secrets \
    external-secrets/external-secrets \
    -n external-secrets \
    --create-namespace \
    --include-crds \
    --set installCRDs=true > giant.yaml
