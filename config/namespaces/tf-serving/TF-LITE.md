# Serving TF Lite Models w/ TF Serving

## Steps
1. Export tflite model
1. Setup tf-serving to use `tflite`
1. Serve model
1. Create simple app that calls tflite with appropriate images
1. Extend app to allow submitting images via browser

## Setup tf-serving to use tflite

OOTB, TF Serving does not support `tflite` models, but an experimental flag has been added allowing this feature.

### Config Flag

Add the following configuration flag to the Deployment

```yaml
    # snippet from deployment
    spec:
      containers:
      - name: resnet-container
        image: tensorflow/serving:2.6.0
        args:
          - --model_config_file=/models/models.config
          - --model_config_file_poll_wait_seconds=60
          - --monitoring_config_file=/monitor/monitoring.config
          - --prefer_tflite_model=true # ENABLE TFLITE as a possible model type to serve
          - --file_system_poll_wait_seconds=300
          - --allow_version_labels_for_unavailable_models=true # Required for multiple models to load

```

> NOTE: This is "experimental" and could be removed at any point

